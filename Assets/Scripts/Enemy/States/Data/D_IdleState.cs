﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newIdleStateData", menuName = "Data/Sate Data/Idle State")]

public class D_IdleState : ScriptableObject
{
    public float minIdleTime = 1f, maxIdleTime = 2f;
}
