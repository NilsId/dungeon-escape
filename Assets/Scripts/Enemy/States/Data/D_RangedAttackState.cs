﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newRangedAttackStateData", menuName = "Data/Sate Data/RangedAttack State")]

public class D_RangedAttackState : ScriptableObject
{
    public GameObject projectile;

    public float projectileDamage = 10f;
    public float projectileSpeed = 12f;
    public float projectileTravelDistance = 0.05f;

    public AudioSource longRangeAttackSound;
}
