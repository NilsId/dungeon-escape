﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDetectedState : State
{
    protected D_PlayerDetected stateData;

    protected bool isPlayerInMinAgroRange, isPlayerInMaxAgroRange;
    protected bool performLongRangeAction, performCloseRangeAction;
    protected bool isDetectingLedge;


    private AudioSource playerDetectedSound;
    private bool canPlayAudio = true;

    public PlayerDetectedState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, D_PlayerDetected stateData) : base(entity, stateMachine, animBoolName)
    {
        this.stateData = stateData;
        playerDetectedSound = GameObject.Instantiate(stateData.playerDetectedSound, entity.aliveGO.transform.position, entity.aliveGO.transform.rotation);
    }

    public override void DoChecks()
    {
        base.DoChecks();

        isPlayerInMinAgroRange = entity.CheckPlayerInMinAgroRange();
        isPlayerInMaxAgroRange = entity.CheckPlayerInMaxAgroRange();
        performCloseRangeAction = entity.CheckPlayerInCloseRangeAction();
        isDetectingLedge = entity.CheckLedge();
    }

    public override void Enter()
    {
        base.Enter();

        entity.SetVelocity(0f);

        if (canPlayAudio)
        {
            playerDetectedSound.Play();
            canPlayAudio = false;
        }

        performLongRangeAction = false;
    }

    public override void Exit()
    {
        base.Exit();

        canPlayAudio = true;
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();

        if(Time.time >= startTime + stateData.longRangeActionTime)
        {
            performLongRangeAction = true;
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();

        playerDetectedSound.transform.position = entity.aliveGO.transform.position;
    }
}
