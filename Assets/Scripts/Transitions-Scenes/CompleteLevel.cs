﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompleteLevel : MonoBehaviour
{
    public GameManager gameManager;

    public GameObject cameraAudio;

    public AudioSource jingle;

    private AudioSource getJingle;

    private void OnTriggerEnter2D(Collider2D target)
    {
        if (target.tag == "Player")
        {
            getJingle = GameObject.Instantiate(jingle, transform.position, transform.rotation);
            cameraAudio.GetComponent<AudioSource>().Stop();
            getJingle.Play();
            gameManager.CompleteLevel();
        }
    }
}
