﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{
    private GameObject[] arrayOfEnemies;

    private float amountOfEnemiesLeft;

    public Text displayEnemies;

    private void Update()
    {
        arrayOfEnemies = GameObject.FindGameObjectsWithTag("EnemiesTag");
        amountOfEnemiesLeft = 18 - arrayOfEnemies.Count();

        displayEnemies.text = "Ennemies killed : " + amountOfEnemiesLeft + "/18";
    }
    public void BackToMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
