﻿using UnityEngine;

public class EndLevel : MonoBehaviour
{
    public GameManager gameManager;

    private void OnTriggerEnter2D(Collider2D target)
    {
        if(target.tag == "Player")
        {
            
            gameManager.CompleteLevel();
        }
        
    }
}
