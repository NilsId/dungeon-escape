﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndLevelTransition : MonoBehaviour
{
    public float timeLeft;

    public Text timeLeftText;
    
    public void LoadMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    private void Update()
    {
        timeLeft -= Time.deltaTime;
        
        timeLeftText.text = "BACK TO MENU IN : " + (timeLeft).ToString("0");
    }
}
